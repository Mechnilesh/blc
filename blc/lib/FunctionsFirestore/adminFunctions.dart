import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import '../main.dart';

int bharta, litti;

TextEditingController bhartaController = TextEditingController();

Future<void> updatePlateSettings(int price, int bharta, int litti) {
  CollectionReference settings =
      FirebaseFirestore.instance.collection('plateSetting');
  return settings
      .doc("7523RFH7SwOSqBrHWyvI")
      .update({'price': price, 'bharta': bharta, 'litti': litti}).then((value) {
    print("updated");
  }).catchError((error) => print("Failed to update settings: $error"));
}

Future<void> updateCustomSettings(int oneBharta, int onelitti) {
  CollectionReference customSettings =
      FirebaseFirestore.instance.collection('customSetting');
  return customSettings
      .doc('cgyG9D7Z7P0JSHuMUDjl')
      .update({'bharta': oneBharta, 'litti': onelitti})
      .then((value) => print("Settings Updated"))
      .catchError((error) => print("Failed to update settings: $error"));
}

Future<void> updateStatus(bool value) {
  CollectionReference status =
      FirebaseFirestore.instance.collection('shopstatus');
  return status
      .doc('SC0jWa6DqbJTVpVVtG6h')
      .update({
        'status': value,
      })
      .then((value) => print("Settings Updated"))
      .catchError((error) => print("Failed to update settings: $error"));
}

//----------------------------------dataRetrieve----------------------------//

Future<void> updateIsProcessing(orderId, acceptClicked, rejectClicked, uid) {
  updateMyOrders(orderId, acceptClicked, uid);
  CollectionReference orders = FirebaseFirestore.instance.collection('orders');

  return orders
      .doc(orderId)
      .update({
        'isProcessing': false,
        'accepted': acceptClicked,
        'rejected': rejectClicked
      })
      .then((value) => print("IsProcess Updated"))
      .catchError((error) => print("Failed to update process: $error"));
}

Future<void> updateMyOrders(orderId, acceptClicked, uid) {
  CollectionReference myorders = FirebaseFirestore.instance
      .collection('users')
      .doc(uid)
      .collection('myOrders');

  return myorders
      .doc(orderId)
      .update({
        // 'isProcessing': false,
        'accepted': acceptClicked,
      })
      .then((value) => print("myOrderprocess Updated"))
      .catchError((error) => print("Failed to update process: $error"));
}
