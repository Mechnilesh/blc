import 'package:blc/Screens/Edit.dart';
import 'package:blc/Screens/Payment.dart';
// import 'package:blc/Screens/YourOrderPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../main.dart';

var timestamp = new DateTime.now();
// bool process = false;

Future<void> addUser(String name, String uid, String email) {
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  return users
      .doc(uid)
      .set({
        'full_name': name,
        'mobile': " ",
        'address': " ",
        'uid': uid,
        'email': email
      })
      .then((value) => print("User Added"))
      .catchError((error) => print("Failed to add user: $error"));
}

Future<void> getData() async {
  return firestore
      .collection('users')
      .where('uid', isEqualTo: auth.currentUser.uid)
      .get()
      .then((QuerySnapshot querySnapshot) => {
            querySnapshot.docs.forEach((doc) {
              print(doc['full_name'] + "1st");
              name = doc['full_name'];
              adr = doc['address'];
              mobile = doc['mobile'];
              email = doc['email'];

              namecontroller.text = doc['full_name'].toString();
              mobilecontroller.text = doc['mobile'].toString();
              addresscontroller.text = doc['address'].toString();
            })
          });
}

Future<void> updateDetails(String name, String mobile, String address) async {
  return firestore.collection('users').doc(auth.currentUser.uid).update({
    'full_name': name,
    'mobile': mobile,
    'address': address,
  });
}

getStatus() {
  FirebaseFirestore.instance
      .collection('shopstatus')
      .doc('SC0jWa6DqbJTVpVVtG6h')
      .get()
      .then((DocumentSnapshot documentSnapshot) {
    if (documentSnapshot.exists) {
      print('Document data: ${documentSnapshot.data()['status']}');
      status = documentSnapshot.data()['status'];
      print("usersone" + status.toString());
    } else {
      print('Document does not exist on the database');
    }
  });
}

Future<void> addorder({
  String name,
  String uid,
  String adr,
  String mobile,
  int total,
  int plates,
  int littiNumber,
  int bhartaNumber,
  var orderId,
}) {
  myorder(plates, littiNumber, bhartaNumber, orderId, total);
  CollectionReference orders = FirebaseFirestore.instance.collection('orders');

  return orders
      .doc(orderId)
      .set({
        'name': name,
        'ordermobile': mobile,
        'orderadr': adr,
        'orderplates': plates,
        'littiNumber': littiNumber,
        'bhartaNumber': bhartaNumber,
        'total': total,
        'uid': uid,
        'timestamp': timestamp,
        'isProcessing': true,
        'orderId': orderId,
        'accepted': false,
        'rejected': false,
        'paid': false,
        'countDown': true,
      })
      .then((value) => print("order Added"))
      .catchError((error) => print("Failed to add order: $error"));
}

Future<void> myorder(
  int plates,
  int littiNumber,
  int bhartaNumber,
  var orderId,
  int total,
) async {
  CollectionReference myOrders = FirebaseFirestore.instance
      .collection('users')
      .doc(auth.currentUser.uid)
      .collection('myOrders');

  return myOrders
      .doc(orderId)
      .set({
        'name': name,
        'ordermobile': mobile,
        'orderadr': adr,
        'orderplates': plates,
        'littiNumber': littiNumber,
        'bhartaNumber': bhartaNumber,
        'total': total,
        'uid': auth.currentUser.uid,
        'timestamp': timestamp,
        'isProcessing': true,
        'orderId': orderId,
        'accepted': false,
        'rejected': false,
        'paid': false,
        'countDown': true,
      })
      .then((value) => print("Myorder Added"))
      .catchError((error) => print("Failed to add order: $error"));
}

//------------------------------------xxxxx-------------------------------------//

Future<void> addorderPaid({
  String name,
  String uid,
  String adr,
  String mobile,
  int total,
  int plates,
  int littiNumber,
  int bhartaNumber,
  var orderId,
}) {
  myPaidorder(plates, littiNumber, bhartaNumber, orderId, total);
  CollectionReference orders = FirebaseFirestore.instance.collection('orders');

  return orders
      .doc(orderId)
      .set({
        'name': name,
        'ordermobile': mobile,
        'orderadr': adr,
        'orderplates': plates,
        'littiNumber': littiNumber,
        'bhartaNumber': bhartaNumber,
        'total': total,
        'uid': uid,
        'timestamp': timestamp,
        'isProcessing': false,
        'orderId': orderId,
        'accepted': true,
        'rejected': false,
        'paid': true,
        'countDown': true,
      })
      .then((value) => print("order Added"))
      .catchError((error) => print("Failed to add order: $error"));
}

Future<void> myPaidorder(
  int plates,
  int littiNumber,
  int bhartaNumber,
  var orderId,
  int total,
) async {
  CollectionReference myOrders = FirebaseFirestore.instance
      .collection('users')
      .doc(auth.currentUser.uid)
      .collection('myOrders');

  return myOrders
      .doc(orderId)
      .set({
        'name': name,
        'ordermobile': mobile,
        'orderadr': adr,
        'orderplates': plates,
        'littiNumber': littiNumber,
        'bhartaNumber': bhartaNumber,
        'total': total,
        'uid': auth.currentUser.uid,
        'timestamp': timestamp,
        'isProcessing': false,
        'orderId': orderId,
        'accepted': true,
        'rejected': false,
        'paid': true,
        'countDown': true,
      })
      .then((value) => print("Myorder Added"))
      .catchError((error) => print("Failed to add order: $error"));
}

//----------------------------------<><>><><><><><><>------------------------------///
// Future<void> updateCountDown() async {
//   print('timer falsed');
//   return firestore
//       .collection('users')
//       .doc(auth.currentUser.uid)
//       .collection('myOrders')
//       .doc(orderId)
//       .update({
//     'countDown': false,
//   });
// }
