import 'package:blc/Screens/AdminPages/AdminSettings.dart';
import 'package:blc/Screens/CustomWidgets/NotificationCard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        title: Text(
          'BLC Admin Panel',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Adminsettings(),
                ),
              );
            },
          ),
        ],
      ),
      body: UserOrderInformation(),
    );
  }
}

class UserOrderInformation extends StatefulWidget {
  @override
  _UserOrderInformationState createState() => _UserOrderInformationState();
}

class _UserOrderInformationState extends State<UserOrderInformation> {
  @override
  Widget build(BuildContext context) {
    Query orders = FirebaseFirestore.instance
        .collection('orders')
        .orderBy('timestamp', descending: true);

    return StreamBuilder<QuerySnapshot>(
      stream: orders.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new PlateOrderCard(
              bhartaNumber: document.data()['bhartaNumber'],
              littiNumber: document.data()['littiNumber'],
              qty: document.data()['orderplates'],
              total: document.data()['total'],
              adr: document.data()['orderadr'],
              mobile: document.data()['ordermobile'],
              name: document.data()['name'],
              orderId: document.data()['orderId'],
              orderAccepted: document.data()['accepted'],
              orderRejected: document.data()['rejected'],
              orderTime: document.data()['timestamp'].toDate(),
              uid:document.data()['uid'],
              isPaid:document.data()['paid'],
            );
          }).toList(),
        );
      },
    );
  }
}
