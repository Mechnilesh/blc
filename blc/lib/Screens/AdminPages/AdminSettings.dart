import 'package:blc/FunctionsFirestore/adminFunctions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../main.dart';

class Adminsettings extends StatefulWidget {
  @override
  _AdminsettingsState createState() => _AdminsettingsState();
}

class _AdminsettingsState extends State<Adminsettings> {
  int platePrice, plateBharta, platelitti;
  int oneBharta, oneLitti;
  bool isUpdating = false;

  TextEditingController platePriceController = TextEditingController();
  TextEditingController bhartaNumberController = TextEditingController();
  TextEditingController littiNumberController = TextEditingController();

  TextEditingController bhartaPriceController = TextEditingController();
  TextEditingController littiPriceController = TextEditingController();

  void initState() {
    super.initState();
    getData();
  }

  getData() {
    FirebaseFirestore.instance
        .collection('plateSetting')
        .doc('7523RFH7SwOSqBrHWyvI')
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        // print('Document data: ${documentSnapshot.data()['bharta']}');
        bhartaNumberController.text =
            documentSnapshot.data()['bharta'].toString();

        platePriceController.text = documentSnapshot.data()['price'].toString();

        littiNumberController.text =
            documentSnapshot.data()['litti'].toString();

        plateBharta = documentSnapshot.data()['bharta'];
        platePrice = documentSnapshot.data()['price'];
        platelitti = documentSnapshot.data()['litti'];
      } else {
        print('Document does not exist on the database');
      }
    });

    FirebaseFirestore.instance
        .collection('customSetting')
        .doc('cgyG9D7Z7P0JSHuMUDjl')
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        // print('Document data: ${documentSnapshot.data()['bharta']}');
        bhartaPriceController.text =
            documentSnapshot.data()['bharta'].toString();

        littiPriceController.text = documentSnapshot.data()['litti'].toString();

        oneLitti = documentSnapshot.data()['litti'];
        oneBharta = documentSnapshot.data()['bharta'];
      } else {
        print('Document does not exist on the database');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        title: Text(
          'Set Pricing',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
              child: isUpdating
                  ? Container(
                      width: 20,
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                    )
                  : GestureDetector(
                      onTap: () async {
                        setState(() {
                          isUpdating = true;
                        });

                        updatePlateSettings(
                          this.platePrice,
                          this.plateBharta,
                          this.platelitti,
                        );
                        updateCustomSettings(
                          this.oneBharta,
                          this.oneLitti,
                        );

                        await Future.delayed(Duration(seconds: 2));

                        setState(() {
                          isUpdating = false;
                        });
                      },
                      child: Text(
                        'Update',
                        style: GoogleFonts.quicksand(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Card(
              child: Container(
                // height: 100,
                // color: Colors.blue,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Plate Setting',
                        style: GoogleFonts.quicksand(
                            // fontWeight: FontWeight.w500,
                            // fontSize: 20,
                            ),
                      ),
                      Image.asset(
                        "assets/plate.png",
                        height: 80,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 80,
                                // color: Colors.grey,
                                child: Text(
                                  'Price : ₹',
                                  style: GoogleFonts.quicksand(
                                      // fontWeight: FontWeight.w500,
                                      // fontSize: 20,
                                      ),
                                ),
                              ),
                              Container(
                                width: 50,
                                child: TextField(
                                  controller: platePriceController,
                                  onChanged: (value) {
                                    platePrice = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: 80,
                                // color: Colors.blue,
                                child: Text(
                                  'Bharta :',
                                  style: GoogleFonts.quicksand(
                                      // fontWeight: FontWeight.w500,
                                      // fontSize: 20,
                                      ),
                                ),
                              ),
                              Container(
                                width: 50,
                                child: TextField(
                                  controller: bhartaNumberController,
                                  onChanged: (value) {
                                    plateBharta = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: 80,
                                // color: Colors.blue,
                                child: Text(
                                  'Litti : ',
                                  style: GoogleFonts.quicksand(
                                      // fontWeight: FontWeight.w500,
                                      // fontSize: 20,
                                      ),
                                ),
                              ),
                              Container(
                                width: 50,
                                child: TextField(
                                  controller: littiNumberController,
                                  onChanged: (value) {
                                    platelitti = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: Container(
                // height: 100,
                // color: Colors.blue,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Custom Setting',
                        style: GoogleFonts.quicksand(
                            // fontWeight: FontWeight.w500,
                            // fontSize: 20,
                            ),
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/custom.png",
                            height: 40,
                          ),
                          Image.asset(
                            "assets/custom1.png",
                            height: 40,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 80,
                                // color: Colors.blue,
                                child: Text(
                                  '1 Bharta : ₹',
                                  style: GoogleFonts.quicksand(
                                      // fontWeight: FontWeight.w500,
                                      // fontSize: 20,
                                      ),
                                ),
                              ),
                              Container(
                                width: 50,
                                child: TextField(
                                  controller: bhartaPriceController,
                                  onChanged: (value) {
                                    oneBharta = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: 80,
                                // color: Colors.blue,
                                child: Text(
                                  '1 Litti : ₹ ',
                                  style: GoogleFonts.quicksand(
                                      // fontWeight: FontWeight.w500,
                                      // fontSize: 20,
                                      ),
                                ),
                              ),
                              Container(
                                width: 50,
                                child: TextField(
                                  controller: littiPriceController,
                                  onChanged: (value) {
                                    oneLitti = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Close Shop/Open shop',
                  style: GoogleFonts.quicksand(),
                ),
                Switch(
                  value: status,
                  onChanged: (value) {
                    setState(() {
                      status = value;
                      print(status);
                      updateStatus(status);
                    });
                  },
                  activeTrackColor: Colors.lightGreenAccent,
                  activeColor: Colors.green,
                  inactiveTrackColor: Colors.red[400],
                  inactiveThumbColor: Colors.red,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
