import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderDetails extends StatefulWidget {
  OrderDetails({
    this.qty,
    this.total,
    this.name,
    this.mobile,
    this.adr,
    this.littiNumber,
    this.bhartaNumber,
    this.isPaid,
  });

  final qty, total, name, mobile, adr, littiNumber, bhartaNumber, isPaid;
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        title: Text(
          'View Order Details',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.yellow,
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: (widget.qty == null)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Litti: ${widget.littiNumber}',
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              'Bharta: ${widget.bhartaNumber}',
                              style: GoogleFonts.quicksand(),
                            ),
                            Text(
                              'Rs: ${widget.total}',
                              style: GoogleFonts.quicksand(),
                            ),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Plates',
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              'Qty: ${widget.qty}',
                              style: GoogleFonts.quicksand(),
                            ),
                            Row(
                              children: [
                                Text(
                                  'Rs: ${widget.total}',
                                  style: GoogleFonts.quicksand(),
                                ),
                                widget.isPaid
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.blue[900],
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              '  Paid  ',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container()
                              ],
                            ),
                          ],
                        ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      '${widget.name}', // name
                      style: GoogleFonts.quicksand(),
                    ),
                  ),
                  Container(
                    child: Text(
                      '${widget.mobile}',
                      style: GoogleFonts.quicksand(),
                    ),
                  ),
                  Container(
                    child: Text(
                      '${widget.adr}',
                      style: GoogleFonts.quicksand(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
