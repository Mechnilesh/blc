// import 'package:blc/FunctionsFirestore/adminFunctions.dart';
// import 'package:blc/Screens/AdminPages/OrderDetails.dart';
// import 'package:circular_countdown_timer/circular_countdown_timer.dart';

// import 'package:blc/FunctionsFirestore/userFunctions.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyOrderCard extends StatefulWidget {
  MyOrderCard({
    this.qty,
    this.total,
    this.name,
    this.mobile,
    this.adr,
    this.littiNumber,
    this.bhartaNumber,
    this.orderId,
    this.orderAccepted,
    this.orderRejected,
    this.time,
    this.firebaseCountDown,
  });
  final qty,
      total,
      name,
      mobile,
      adr,
      littiNumber,
      bhartaNumber,
      orderId,
      orderAccepted,
      orderRejected;
  final time;
  final firebaseCountDown;
  @override
  _MyOrderCardState createState() => _MyOrderCardState();
}

class _MyOrderCardState extends State<MyOrderCard> {
  // bool acceptClicked = false;
  // bool rejectClicked = false;
  // final _key = GlobalKey();
  // CountDownController _controller = CountDownController();
  // bool _isPause = false;
  bool isCountDownOver = false;

  @override
  Widget build(BuildContext context) {
    // acceptClicked = widget.orderAccepted;
    // rejectClicked = widget.orderRejected;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Column(
        children: [
          Divider(
            color: Colors.yellowAccent[400],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  (widget.qty == null)
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Litti: ${widget.littiNumber}',
                              style: GoogleFonts.quicksand(
                                  // fontWeight: FontWeight.w500,
                                  ),
                            ),
                            Text(
                              'Bharta: ${widget.bhartaNumber}',
                              style: GoogleFonts.quicksand(),
                            ),
                            Text(
                              'total ₹: ${widget.total}',
                              style: GoogleFonts.quicksand(),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Plates : ${widget.qty}',
                              style: GoogleFonts.quicksand(
                                  // fontWeight: FontWeight.w500,
                                  ),
                            ),
                            Text(
                              'total ₹: ${widget.total}',
                              style: GoogleFonts.quicksand(
                                  // fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ],
                        ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                // color: Colors.grey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      '${widget.name}'.toUpperCase(),
                      style: GoogleFonts.quicksand(),
                    ),
                    Text(
                      '${widget.mobile}',
                      style: GoogleFonts.quicksand(),
                    ),
                    Text(
                      '${widget.adr}',
                      style: GoogleFonts.quicksand(),
                    ),
                    Text(
                      '${widget.time}',
                      style: GoogleFonts.quicksand(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              widget.orderAccepted
                  // ? isCountDownOver
                  ? Icon(
                      Icons.check_circle_outline,
                      color: Colors.green,
                    )
                  // : widget.firebaseCountDown
                  //     ? CircularCountDownTimer(
                  //       key: _key,
                  //         width: 60,
                  //         height: 50,
                  //         duration: 60,
                  //         fillColor: Colors.yellowAccent[400],
                  //         color: Colors.green,
                  //         strokeWidth: 2,
                  //         isReverse: true,
                  //         isReverseAnimation: true,
                  //         textStyle: TextStyle(fontSize: 12),
                  //         controller: _controller,
                  //         onComplete: () async {
                  //           updateCountDown();
                  //           setState(() {
                  //             isCountDownOver = true;
                  //           });
                  //         },
                  //       )
                  //     : Icon(
                  //         Icons.check_circle_outline,
                  //         color: Colors.green,
                  //       )
                  : Icon(
                      Icons.cancel,
                      color: Colors.red,
                    ),
            ],
          ),
          Divider(
            color: Colors.yellowAccent[400],
          ),
        ],
      ),
    );
  }
}
