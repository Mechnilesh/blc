import 'package:blc/FunctionsFirestore/adminFunctions.dart';
import 'package:blc/Screens/AdminPages/OrderDetails.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PlateOrderCard extends StatefulWidget {
  PlateOrderCard({
    this.qty,
    this.total,
    this.name,
    this.mobile,
    this.adr,
    this.littiNumber,
    this.bhartaNumber,
    this.orderId,
    this.orderAccepted,
    this.orderRejected,
    this.orderTime,
    this.uid,
    this.isPaid,
  });
  final qty,
      total,
      name,
      mobile,
      adr,
      littiNumber,
      bhartaNumber,
      orderId,
      orderAccepted,
      orderRejected;
  final orderTime;
  final uid;
  final isPaid;
  @override
  _PlateOrderCardState createState() => _PlateOrderCardState();
}

class _PlateOrderCardState extends State<PlateOrderCard> {
  bool acceptClicked = false;
  bool rejectClicked = false;

  Widget build(BuildContext context) {
    acceptClicked = widget.orderAccepted;
    rejectClicked = widget.orderRejected;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        height: 120,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            (widget.qty == null)
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Litti: ${widget.littiNumber}',
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Bharta: ${widget.bhartaNumber}',
                        style: GoogleFonts.quicksand(),
                      ),
                      Text(
                        'Rs: ${widget.total}',
                        style: GoogleFonts.quicksand(),
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Plates',
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Qty: ${widget.qty}',
                        style: GoogleFonts.quicksand(),
                      ),
                      Text(
                        'Rs: ${widget.total}',
                        style: GoogleFonts.quicksand(),
                      ),
                    ],
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                rejectClicked
                    ? Icon(
                        Icons.cancel_rounded,
                        color: Colors.red,
                      )
                    : acceptClicked
                        ? Container()
                        // ignore: deprecated_member_use
                        : FlatButton(
                            onPressed: () {
                              updateIsProcessing(
                                widget.orderId,
                                acceptClicked,
                                rejectClicked = true,
                                widget.uid,
                              );
                              setState(() {
                                rejectClicked = true;
                              });
                            },
                            child: Text(
                              'Reject',
                              style: GoogleFonts.quicksand(color: Colors.white),
                            ),
                            color: Colors.red,
                          ),
                acceptClicked
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Icon(
                            Icons.check_circle_outline,
                            color: Colors.green,
                          ),
                          widget.isPaid
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blue[900],
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(
                                        '  Paid  ',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      )
                    : rejectClicked
                        ? Container()
                        // ignore: deprecated_member_use
                        : FlatButton(
                            onPressed: () {
                              updateIsProcessing(
                                widget.orderId,
                                acceptClicked = true,
                                rejectClicked,
                                widget.uid,
                              );
                              setState(() {
                                acceptClicked = true;
                                // rejectClicked = true;
                              });
                            },
                            child: Text(
                              'Accept',
                              style: GoogleFonts.quicksand(color: Colors.white),
                            ),
                            color: Colors.green,
                          ),
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OrderDetails(
                          qty: widget.qty,
                          total: widget.total,
                          name: widget.name,
                          mobile: widget.mobile,
                          adr: widget.adr,
                          littiNumber: widget.littiNumber,
                          bhartaNumber: widget.bhartaNumber,
                          isPaid: widget.isPaid,
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'View Details',
                    style: GoogleFonts.quicksand(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  color: Colors.yellowAccent[400],
                ),
              ],
            ),
            Text(
              '${widget.orderTime}',
              style: GoogleFonts.quicksand(color: Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}
