import 'dart:math';

import 'package:blc/FunctionsFirestore/userFunctions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

// import 'package:flutter_sms/flutter_sms.dart';

// import 'package:flutter_otp/flutter_otp.dart';
// import 'package:sms/sms.dart';

// FirebaseAuth auth = FirebaseAuth.instance;

TextEditingController namecontroller = TextEditingController();
TextEditingController mobilecontroller = TextEditingController();
TextEditingController addresscontroller = TextEditingController();

class EditPage extends StatefulWidget {
  final Function refresh;
  EditPage(this.refresh); //co
  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  String name = namecontroller.text.trimLeft(),
      address = addresscontroller.text.trimLeft();
  String mobile = mobilecontroller.text.trimLeft();
  bool _validate = false;
  bool isloading = false;
  bool optField = false;
  bool sendOtpButton = false;
  bool loadingOtp = false;
  bool correct = false;
  bool showtick = true;

  void initState() {
    super.initState();
    getAsyncData();
    // updateDetails();
  }

  getAsyncData() async {
    await getData();
    // print(name);
    setState(() {});
  }

  //----------------------------Mobile no. verification----------------//
  // SmsSender sender = new SmsSender();
  void sendOtp() {
    // sender.sendSms(new SmsMessage(mobile, 'Your BLC OTP is : $_sendOtp'));
  }

  // FlutterOtp otp = FlutterOtp();
  // int _sendOtp;

  void randomCode() async {
    int min = 1000, max = 9999;
    Random rnd = new Random();
    int code = min + rnd.nextInt(max - min);
    print("Your OTP is : " + code.toString());
    // _sendOtp = code;
  }

  // votp() async {
  //   await auth.verifyPhoneNumber(
  //     phoneNumber: mobile,
  //     codeSent: (String verificationId, int resendToken) async {
  //       // Update the UI - wait for the user to enter the SMS code
  //       String smsCode = 'xxxx';

  //       // Create a PhoneAuthCredential with the code
  //       PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
  //           verificationId: verificationId, smsCode: smsCode);

  //       // Sign the user in (or link) with the credential
  //       await auth.signInWithCredential(phoneAuthCredential);
  //     },
  //   );
  // }

  //--------------------------<><><><><><><><><><><>-------------------//

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        elevation: 0,
        title: Text(
          'Update details',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Container(
            height: 30,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 0.5,
                ),
              ],
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 16,
              ),
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      backgroundColor: Colors.yellowAccent[400],
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
        ),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextField(
                    controller: namecontroller,
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      hintText: 'Type your full name...',
                      labelText: 'Name',
                      hintStyle: GoogleFonts.quicksand(),
                      labelStyle: GoogleFonts.poppins(color: Colors.orange),
                      // errorText: _validate ? "Value can't be empty" : null,
                    ),
                    onChanged: (value) {
                      name = value.trimLeft();
                    },
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextFormField(
                    controller: addresscontroller,
                    decoration: InputDecoration(
                      hintText: 'Type your address...',
                      labelText: 'Delivery address',
                      hintStyle: GoogleFonts.quicksand(),
                      labelStyle: GoogleFonts.poppins(
                        color: Colors.orange,
                      ),
                    ),
                    onChanged: (value) {
                      address = value.trimLeft();
                      print(address);
                    },
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: TextFormField(
                        // inputFormatters: [RegExp('')],
                        controller: mobilecontroller,
                        cursorColor: Colors.orange,
                        keyboardType: TextInputType.phone,
                        maxLength: 10,
                        decoration: InputDecoration(
                          border: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.orange),
                          ),
                          hintText: 'Type your mobile no...',
                          labelText: 'Mobile number',
                          hintStyle: GoogleFonts.quicksand(),
                          labelStyle: GoogleFonts.poppins(
                            color: Colors.orange,
                          ),
                          // errorText: _validate ? "Value can't be empty" : null,
                        ),
                        onChanged: (value) {
                          mobile = value.trimLeft();
                          if (value.length == 10) {
                            setState(() {
                              sendOtpButton = true;
                            });
                          } else {
                            setState(() {
                              sendOtpButton = false;
                            });
                          }
                          // print(mobile);
                        },
                      ),
                    ),
                    // sendOtpButton
                    //     ? loadingOtp
                    //         ? Padding(
                    //             padding: const EdgeInsets.all(8.0),
                    //             child: CircularProgressIndicator(),
                    //           )
                    //         : FlatButton(
                    //             splashColor: Colors.green,
                    //             shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.circular(18.0),
                    //             ),
                    //             onPressed: () async {
                    //               // int minNumber = 1000;
                    //               // int maxNumber = 6000;
                    //               // String countryCode = "+91";
                    //               // otp.sendOtp(
                    //               //   mobile,
                    //               //   'YOUR BLC OTP : ',
                    //               //   minNumber,
                    //               //   maxNumber,
                    //               //   countryCode,
                    //               // );

                    //               randomCode();
                    //               // sendOtp();

                    //               setState(() {
                    //                 loadingOtp = true;
                    //                 optField = true;
                    //                 showtick = false;
                    //               });

                    //               await Future.delayed(Duration(seconds: 4));

                    //               setState(() {
                    //                 loadingOtp = false;
                    //               });
                    //             },
                    //             child: Center(
                    //               child: Padding(
                    //                 padding: const EdgeInsets.all(1.0),
                    //                 child: Text(
                    //                   'Send OTP',
                    //                   style: GoogleFonts.quicksand(
                    //                     fontWeight: FontWeight.bold,
                    //                     color: Colors.blue,
                    //                   ),
                    //                 ),
                    //               ),
                    //             ),
                    //           )
                    //     : Container(
                    //         width: MediaQuery.of(context).size.width * 0.25,
                    //       ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                    ),
                  ],
                ),
                optField
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: TextFormField(
                              cursorColor: Colors.orange,
                              keyboardType: TextInputType.phone,
                              maxLength: 4,
                              decoration: InputDecoration(
                                border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.orange),
                                ),
                                hintText: 'OTP',
                                labelText: 'OTP',
                                hintStyle: GoogleFonts.quicksand(),
                                labelStyle: GoogleFonts.poppins(
                                  color: Colors.orange,
                                ),
                              ),
                              onChanged: (value) {
                                // correct = otp
                                //     .resultChecker(int.parse(value.trimLeft()));
                                // print(correct);
                                // if (correct) {
                                //   setState(() {
                                //     showtick = true;
                                //   });
                                // } else {
                                //   setState(() {
                                //     showtick = false;
                                //   });
                                // }
                              },
                            ),
                          ),
                          Icon(
                            Icons.check_circle_outline,
                            color: showtick ? Colors.green : Colors.white,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.36,
                          ),
                        ],
                      )
                    : Container(),
                _validate
                    ? Padding(
                        padding: const EdgeInsets.only(top: 50),
                        child: Center(
                          child: Text(
                            showtick ? "Fields can't be empty" : "OTP is Wrong",
                            style: TextStyle(
                              color: Colors.red,
                            ),
                          ),
                        ),
                      )
                    : Container(),
                isloading
                    ? Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                        // ignore: deprecated_member_use
                        child: FlatButton(
                          splashColor: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () async {
                            if ((name.isEmpty || mobile.isEmpty || !showtick) ||
                                address.isEmpty) {
                              setState(() {
                                _validate = true;
                              });
                            } else {
                              setState(() {
                                _validate = false;
                                isloading = true;
                              });

                              await updateDetails(name, mobile, address);
                              widget.refresh();
                              print('refresh e');
                              Navigator.pop(context);
                            }
                          },
                          child: Container(
                            child: Center(
                              child: Text(
                                'Save and Update',
                                style: GoogleFonts.quicksand(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            height: 40,
                            width: MediaQuery.of(context).size.width * 0.7,
                            decoration: BoxDecoration(
                              color: Colors.yellowAccent[400],
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
