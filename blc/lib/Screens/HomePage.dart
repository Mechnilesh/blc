import 'package:blc/FunctionsFirestore/userFunctions.dart';
// import 'package:blc/Screens/AdminPages/AdminPage.dart';

import 'package:blc/Screens/Edit.dart';
import 'package:blc/Screens/OrderPage.dart';
import 'package:blc/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'Payment.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'YourOrderPage.dart';

class HomePage extends StatefulWidget {
  HomePage({
    this.currentUser,
  });

  final String currentUser;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int bhartaNumber = ibn, platePrice = ipp, littiNumber = iln;
  int bhartaPrice = ibp, littiPrice = ilp;
  bool isConfirm = false;

  Function get refresh => () {
        Navigator.pop(context);
      };

  void initState() {
    super.initState();
    connected();
    getThisData();
    getStatus();
  }

  connected() async {
    DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
          print('Data connection is available.');
          internetConnected = true;
          setState(() {});

          break;
        case DataConnectionStatus.disconnected:
          print('You are disconnected from the internet.');
          internetConnected = false;
          setState(() {});
          break;
      }
    });
  }

  getThisData() {
    // CollectionReference customSetting =
    //     FirebaseFirestore.instance.collection('customSetting');
    getData();

    FirebaseFirestore.instance
        .collection('plateSetting')
        .doc('7523RFH7SwOSqBrHWyvI')
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        // print('Document data: ${documentSnapshot.data()['bharta']}');
        bhartaNumber = documentSnapshot.data()['bharta'];

        platePrice = documentSnapshot.data()['price'];

        littiNumber = documentSnapshot.data()['litti'];
        setState(() {});
      } else {
        print('Document does not exist on the database');
      }
    });

    FirebaseFirestore.instance
        .collection('customSetting')
        .doc('cgyG9D7Z7P0JSHuMUDjl')
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        // print('Document data: ${documentSnapshot.data()['bharta']}');
        bhartaPrice = documentSnapshot.data()['bharta'];

        littiPrice = documentSnapshot.data()['litti'];
        setState(() {});
      } else {
        print('Document does not exist on the database');
      }
    });
  }

  signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => AuthPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        elevation: 0,
        title: Text(
          'Choose Order Type',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        // actions: [
        //   IconButton(
        //     icon: Icon(Icons.admin_panel_settings),
        //     onPressed: () {
        //       Navigator.push(
        //         context,
        //         MaterialPageRoute(
        //           builder: (context) => AdminPage(),
        //         ),
        //       );
        //     },
        //   )
        // ],
      ),
      drawer: SingleChildScrollView(
        child: GestureDetector(
          onTap: () {
            setState(() {
              isConfirm = false;
            });
          },
          child: Container(
            width: MediaQuery.of(context).size.width * 0.5,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.yellowAccent[400],
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(100),
                bottomRight: Radius.circular(100),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/wlscreen.png',
                        height: 200,
                      ),
                    ),
                    Text(
                      email.toString(),
                      style: GoogleFonts.quicksand(),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Divider(
                      color: Colors.white,
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditPage(refresh),
                          ),
                        );
                      },
                      title: Text(
                        'Edit Your Details',
                        style: GoogleFonts.quicksand(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.white,
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => YourOrderPage(),
                          ),
                        );
                      },
                      title: Text(
                        'Your Orders',
                        style: GoogleFonts.quicksand(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.white,
                    ),
                    isConfirm
                        ? Container(
                            // color: Colors.white,
                            height: 55,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.check_circle_outline),
                                  onPressed: () {
                                    signOut();
                                  },
                                ),
                                IconButton(
                                  icon: Icon(Icons.cancel),
                                  onPressed: () {
                                    setState(() {
                                      isConfirm = false;
                                    });
                                  },
                                )
                              ],
                            ),
                          )
                        : ListTile(
                            onTap: () {
                              setState(() {
                                isConfirm = true;
                              });
                            },
                            title: Text(
                              'Log Out',
                              style: GoogleFonts.quicksand(
                                fontSize: 18,
                              ),
                            ),
                            // tileColor: Colors.white,
                          ),
                    ListTile(
                      title: Text(
                        'v1.0.3',
                        style: GoogleFonts.quicksand(
                          fontSize: 10,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        color: Colors.yellowAccent[400],
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Center(
            child: Stack(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //----------------------------------Contaner_1-----------------------------//

                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.74,
                        width: MediaQuery.of(context).size.width * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 0.5,
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Order plates',
                                style: GoogleFonts.quicksand(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            Hero(
                              tag: 'h2',
                              child: Image.asset(
                                "assets/plateImage.jpg",
                              ),
                            ),
                            Text(
                              '₹ $platePrice',
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              '/ One plate',
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                '$littiNumber Baati(litti) and $bhartaNumber bowl Bharta',
                                style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: internetConnected
                                  ? Hero(
                                      tag: 'h1',
                                      // ignore: deprecated_member_use
                                      child: FlatButton(
                                        splashColor: Colors.yellowAccent[400],
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side:
                                              BorderSide(color: Colors.orange),
                                        ),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => BuyPlates(
                                                platePrice: platePrice,
                                                littiNumber: littiNumber,
                                                bhartaNumber: bhartaNumber,
                                                // currentUser: widget.currentUser,
                                              ),
                                            ),
                                          );
                                        },
                                        child: Container(
                                          child: Center(
                                            child: Text(
                                              'Order',
                                              style: GoogleFonts.quicksand(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          height: 30,
                                          width: 80,
                                          decoration: BoxDecoration(
                                            color: Colors.yellowAccent[400],
                                            borderRadius:
                                                BorderRadius.circular(40),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Text(
                                      'No Internet found!',
                                      style: TextStyle(color: Colors.red),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    //-------------------------------------------------------------------//

                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.08,
                      child: Icon(Icons.compare_arrows),
                    ),

                    //----------------------------------Container_2---------------------//

                    Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.74,
                        width: MediaQuery.of(context).size.width * 0.8,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 0.5,
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Order Custom',
                                style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w500, fontSize: 20),
                              ),
                            ),
                            Hero(
                              tag: 'h4',
                              child: Image.asset(
                                "assets/customImage.jpg",
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: Text(
                                'Order Number of Baaties(litties) ',
                                style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Text(
                              'and bhartas manualy',
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                '',
                                style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            internetConnected
                                ? Hero(
                                    tag: 'h3',
                                    // ignore: deprecated_member_use
                                    child: FlatButton(
                                      splashColor: Colors.yellowAccent[400],
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: BorderSide(color: Colors.orange),
                                      ),
                                      color: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => BuyCustoms(
                                                bhartaPrice: bhartaPrice,
                                                littiPrice: littiPrice),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        child: Center(
                                          child: Text(
                                            'Order',
                                            style: GoogleFonts.quicksand(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        height: 30,
                                        width: 80,
                                        decoration: BoxDecoration(
                                          color: Colors.yellowAccent[400],
                                          borderRadius:
                                              BorderRadius.circular(40),
                                        ),
                                      ),
                                    ),
                                  )
                                : Text(
                                    'No Internet found!',
                                    style: TextStyle(color: Colors.red),
                                  )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
            
              ],
            ),
          ),
        ),
      ),
    );
  }
}
