import 'package:blc/Screens/Edit.dart';
import 'package:blc/Screens/Payment.dart';
import 'package:blc/main.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:blc/FunctionsFirestore/userFunctions.dart';

bool payPageTwo = false;

class BuyPlates extends StatefulWidget {
  BuyPlates({
    this.platePrice,
    this.bhartaNumber,
    this.littiNumber,
    // this.currentUser,
  });
  final int platePrice, bhartaNumber, littiNumber;
  // final String currentUser;

  @override
  _BuyPlatesState createState() => _BuyPlatesState();
}

class _BuyPlatesState extends State<BuyPlates> {
  List qt = [1];
  bool isEdit = false;
  // int price = widget.platePrice;

  void initState() {
    super.initState();
    getAsyncData();
    // updateDetails();
  }

  getAsyncData() async {
    await getData();
    if (adr.trimLeft().isEmpty || mobile.trimLeft().isEmpty) {
      setState(() {
        isEdit = true;
        print('isedit true');
      });
    } else {
      setState(() {
        isEdit = false;
        print('isedit false');
      });
    }
  }

  refresh() {
    setState(() {
      getAsyncData();
    });
    print('refresh 1');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellowAccent[400],
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        elevation: 0,
        title: Text(
          'Order Setup',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Container(
            height: MediaQuery.of(context).size.height * 0.05,
            width: MediaQuery.of(context).size.width * 0.1,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 0.5,
                ),
              ],
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 16,
              ),
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.7,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset(
                    'assets/d.png',
                    scale: 4,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        'Number of Plates to be order',
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      // SizedBox(
                      //   width: 60,
                      // ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.2,
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (qt[0] != 1)
                                setState(() {
                                  qt[0] -= 1;
                                });
                            },
                            child: Icon(
                              Icons.remove,
                              color: Colors.grey,
                              size: 20,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, right: 3),
                            child: Text(
                              '${qt[0]}',
                              style: GoogleFonts.quicksand(
                                fontSize: 16,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                qt[0] += 1;
                              });
                            },
                            child: Icon(
                              Icons.add,
                              color: Colors.orange,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.orange),
                    ),
                    height: 150,
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Hero(
                          tag: 'h2',
                          child: Image.asset(
                            'assets/plate.png',
                            height: 100,
                          ),
                        ),
                        Text(
                          ' ₹ ${widget.platePrice}',
                          style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '/ One plate',
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w500,
                            fontSize: 10,
                          ),
                        ),
                        Text(
                          '${widget.littiNumber} Baati(litti) and ${widget.bhartaNumber} bowl Bharta',
                          style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w500,
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "'Enjoy your favourite Bihari litti chokha at home'",
                        style: GoogleFonts.poppins(
                            fontStyle: FontStyle.italic, fontSize: 12),
                      ),
                      Text(
                        'BLC provides homemade litti chokha  :) ',
                        style: GoogleFonts.poppins(
                          fontStyle: FontStyle.italic,
                          fontSize: 12,
                          color: Colors.yellowAccent[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                ],
              ),
            ),
          ),
          status
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Total price',
                              style: GoogleFonts.quicksand(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              '₹${widget.platePrice * qt[0]}',
                              style: GoogleFonts.roboto(
                                color: Colors.yellowAccent[400],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.06,
                        ),
                        Hero(
                          tag: 'h1',
                          // ignore: deprecated_member_use
                          child: FlatButton(
                            splashColor: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.green),
                            ),
                            onPressed: () {
                              setState(() {
                                payPageTwo = false;
                              });
                              //------------------------->>
                              if (isEdit == true) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EditPage(refresh),
                                  ),
                                ).then(
                                  (value) => setState(() {}),
                                );
                              } else
                                Navigator.of(context).push(
                                  new PayMentPageRoute(
                                    qt: qt[0],
                                    price: widget.platePrice,
                                  ),
                                );
                            },
                            child: Container(
                              child: Center(
                                child: Text(
                                  'Buy',
                                  style: GoogleFonts.quicksand(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              height: 30,
                              width: 80,
                              decoration: BoxDecoration(
                                color: Colors.yellowAccent[400],
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              : Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'BLC is temporarily closed :)',
                          style: GoogleFonts.quicksand(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

//------------------------------------------------BuyPage 2--------------------------//

//------------------------------------------------BuyPage 2--------------------------//

//------------------------------------------------BuyPage 2--------------------------//

class BuyCustoms extends StatefulWidget {
  BuyCustoms({this.bhartaPrice, this.littiPrice});
  final bhartaPrice, littiPrice;
  @override
  _BuyCustomsState createState() => _BuyCustomsState();
}

class _BuyCustomsState extends State<BuyCustoms> {
  List qt = [3, 1];

  bool isEdit = false;

  void initState() {
    super.initState();
    getAsyncData();
    // updateDetails();
  }

  getAsyncData() async {
    await getData();
    if (adr.trimLeft().isEmpty || mobile.trimLeft().isEmpty) {
      setState(() {
        isEdit = true;
        print('isedit true');
      });
    } else {
      setState(() {
        isEdit = false;
        print('isedit false');
      });
    }
  }

  refresh() {
    setState(() {
      getAsyncData();
    });
    print('refresh 1');
  }

  @override
  Widget build(BuildContext context) {
    int totalPrice = (widget.littiPrice * qt[0]) + (widget.bhartaPrice * qt[1]);
    return Scaffold(
      backgroundColor: Colors.yellowAccent[400],
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        elevation: 0,
        title: Text(
          'Custom Order Setup',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Container(
            height: 30,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 0.5,
                ),
              ],
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 16,
              ),
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.7,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Image.asset(
                      'assets/d.png',
                      scale: 4,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: [
                          Text(
                            'Number of Litties(baati)',
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            'note: minimum 3 litties',
                            style: GoogleFonts.poppins(
                                fontStyle: FontStyle.italic, fontSize: 10),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.2,
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (qt[0] != 3)
                                setState(() {
                                  qt[0] -= 1;
                                });
                            },
                            child: Icon(
                              Icons.remove,
                              color: Colors.grey,
                              size: 20,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, right: 3),
                            child: Text(
                              '${qt[0]}',
                              style: GoogleFonts.quicksand(
                                fontSize: 16,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                qt[0] += 1;
                              });
                            },
                            child: Icon(
                              Icons.add,
                              color: Colors.orange,
                              size: 30,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  // Text(
                  //   'note: minimum 3 litties',
                  //   style: GoogleFonts.poppins(
                  //       fontStyle: FontStyle.italic, fontSize: 10),
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        'Number of Bhrta(chokha) bowls',
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.06,
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (qt[1] != 1)
                                setState(() {
                                  qt[1] -= 1;
                                });
                            },
                            child: Icon(
                              Icons.remove,
                              color: Colors.grey,
                              size: 20,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, right: 3),
                            child: Text(
                              '${qt[1]}',
                              style: GoogleFonts.quicksand(
                                fontSize: 16,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                qt[1] += 1;
                              });
                            },
                            child: Icon(
                              Icons.add,
                              color: Colors.orange,
                              size: 30,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.orange),
                    ),
                    height: 150,
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 25),
                            child: Hero(
                              tag: 'h4',
                              child: Row(
                                children: <Widget>[
                                  Image.asset(
                                    "assets/custom1.png",
                                    height: 40,
                                  ),
                                  Image.asset(
                                    "assets/custom.png",
                                    height: 80,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'one litti ',
                                style: GoogleFonts.roboto(
                                    // fontWeight: FontWeight.w500,
                                    ),
                              ),
                              SizedBox(
                                width: 53,
                              ),
                              Text(
                                '₹${widget.littiPrice}.0',
                                style: GoogleFonts.roboto(
                                    // fontWeight: FontWeight.w500,
                                    ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'one bowl bhrta',
                                style: GoogleFonts.roboto(
                                    // fontWeight: FontWeight.w500,
                                    // fontSize: 10,
                                    ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.03,
                              ),
                              Text(
                                '₹${widget.bhartaPrice}.0',
                                style: GoogleFonts.roboto(
                                    // fontWeight: FontWeight.w500,
                                    // fontSize: 10,
                                    ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                ],
              ),
            ),
          ),
          status
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Total price',
                              style: GoogleFonts.quicksand(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              '₹${(widget.littiPrice * qt[0]) + (widget.bhartaPrice * qt[1])}',
                              style: GoogleFonts.roboto(
                                color: Colors.yellowAccent[400],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.06,
                        ),
                        Hero(
                          tag: 'h3',
                          // ignore: deprecated_member_use
                          child: FlatButton(
                            splashColor: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.green),
                            ),
                            onPressed: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) => PaymentPageTwo(),
                              //   ),
                              // );
                              setState(() {
                                payPageTwo = true;
                              });
                              if (isEdit == true) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EditPage(refresh),
                                  ),
                                ).then(
                                  (value) => setState(() {}),
                                );
                              } else
                                Navigator.of(context).push(
                                  new PayMentPageRoute(
                                    // qt: qt[0],
                                    // price: widget.platePrice,
                                    price: totalPrice,
                                    qt: 0,
                                    littiNumber: qt[0],
                                    bhartaNumber: qt[1],
                                  ),
                                );
                            },
                            child: Container(
                              child: Center(
                                child: Text(
                                  'Buy',
                                  style: GoogleFonts.quicksand(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              height: 30,
                              width: 80,
                              decoration: BoxDecoration(
                                color: Colors.yellowAccent[400],
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              : Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'BLC is temporarily closed :)',
                          style: GoogleFonts.quicksand(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
