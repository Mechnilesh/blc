import 'dart:async';
import 'dart:ui';

import 'package:blc/FunctionsFirestore/userFunctions.dart';
import 'package:blc/Screens/Edit.dart';
import 'package:blc/Screens/orderStatus.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import 'package:uuid/uuid.dart';
import 'OrderPage.dart';

import '../main.dart';

String name;
String mobile = "", adr = "";
String email;
var orderId;
var uuid = Uuid();

class PayMentPageRoute<T> extends PageRoute<T> {
  PayMentPageRoute({this.qt, this.price, this.littiNumber, this.bhartaNumber});
  final int qt, price, littiNumber, bhartaNumber;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return FadeTransition(
      opacity: animation,
      child: new PayMentPage(
        qt: qt,
        price: price,
        littiNumber: littiNumber,
        bhartaNumber: bhartaNumber,
      ),
    );
  }

  @override
  Color get barrierColor => Colors.black;

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration(milliseconds: 500);
}

class PayMentPage extends StatefulWidget {
  PayMentPage({
    this.qt,
    this.price,
    this.littiNumber,
    this.bhartaNumber,
  });

  final int qt, price, littiNumber, bhartaNumber;

  @override
  _PayMentPageState createState() => _PayMentPageState(
        qt: qt,
        price: price,
        littiNumber: littiNumber,
        bhartaNumber: bhartaNumber,
      );
}

class _PayMentPageState extends State<PayMentPage> {
//----------------------------RazorPayMethod--------------------------//
  Razorpay _razorpay;

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) async {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Colors.green,
        content: Text(
          "Payment Successful",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 4),
      ),
    );
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => OrderAcceptedPage()),
    );
    orderId = uuid.v4();
    print(orderId);
    payPageTwo
        ? await addorderPaid(
            mobile: mobile,
            name: name,
            adr: adr,
            littiNumber: littiNumber,
            bhartaNumber: bhartaNumber,
            total: price,
            uid: auth.currentUser.uid,
            orderId: orderId,
          )
        : await addorderPaid(
            mobile: mobile,
            name: name,
            adr: adr,
            plates: qt,
            total: total,
            uid: auth.currentUser.uid,
            orderId: orderId,
          );
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Colors.black,
        content: Text(
          "Try again or another method",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 4),
      ),
    );
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          "EXTERNAL_WALLET: " + response.walletName,
        ),
        duration: Duration(seconds: 4),
      ),
    );
  }

//<><><><><><><><><><><><><><>RazorPayMethod<><><><><><><><><><><><><><>//

  void initState() {
    super.initState();
    isInternet();
    getAsyncData();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  isInternet() async {
    {
      DataConnectionChecker().onStatusChange.listen((status) {
        switch (status) {
          case DataConnectionStatus.connected:
            print('Data connection is available.');
            internetConnected = true;
            setState(() {});

            break;
          case DataConnectionStatus.disconnected:
            print('You are disconnected from the internet.');
            internetConnected = false;
            setState(() {});
            break;
        }
      });
    }
  }

  getAsyncData() async {
    await getData();
    // print(name);
    // print('s ooooo');
    setState(() {});
  }

  refresh() {
    // print('s o');
    getAsyncData();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _PayMentPageState({
    this.qt,
    this.price,
    this.littiNumber,
    this.bhartaNumber,
  });

  final int qt, price, littiNumber, bhartaNumber;
  int total;
  Timer timerControl;

  bool isOrdering = false;
  bool startTimer = true;
  bool accepted = false;
  bool rejected = false;
  bool cashOnDel = false;

  Future<void> getOrderData() async {
    return firestore
        .collection('orders')
        .where('orderId', isEqualTo: orderId)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                isOrdering = doc['isProcessing'];
                if (isOrdering == false) {
                  timerControl.cancel();
                }

                accepted = doc['accepted'];
                rejected = doc['rejected'];

                setState(() {});
              })
            });
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_live_cfVTzXkaFIvayN',
      'amount': payPageTwo ? price * 100 : total * 100,
      'name': name,
      'description': 'Pay to BLC',
      'prefill': {'contact': mobile, 'email': email},
      'external': {
        'wallets': ['']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    total = qt * price;
    if (accepted) {
      timerControl.cancel();
      return OrderAcceptedPage();
    } else if (rejected) {
      timerControl.cancel();
      return OrderRejectedPage();
    } else
      return WillPopScope(
        onWillPop: () async => isOrdering ? false : true,
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.black,

          appBar: AppBar(
            backgroundColor: Colors.black,
            elevation: 0,
            title: Text(
              'Order details',
              style: GoogleFonts.quicksand(
                color: Colors.yellowAccent[400],
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
            leading: IconButton(
              icon: Container(
                height: 30,
                width: 40,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 0.5,
                    ),
                  ],
                ),
                child: Center(
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                    size: 16,
                  ),
                ),
              ),
              onPressed: () => isOrdering ? null : Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: GestureDetector(
                  onTap: () {
                    isOrdering
                        ? Container()
                        : Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => EditPage(refresh),
                            ),
                          );
                  },
                  child: Icon(Icons.edit),
                ),
              ),
            ],
          ),

          //-----------------------------------------Body----------------------------------//

          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.78,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(40),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'assets/d.png',
                      scale: 4,
                    ),
                    Text(
                      '>--Delivery Details --<',
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 28.0, top: 0),
                        child: Text(
                          '$name'.toUpperCase(),
                          style: GoogleFonts.quicksand(),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 28.0, top: 0),
                        child: Row(
                          children: [
                            Text(
                              'Your mobile number : ',
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              '$mobile',
                              style: GoogleFonts.quicksand(),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 28.0, top: 0),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: MediaQuery.of(context).size.width * 0.8,
                          // color: Colors.green,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Deliver to :',
                                style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                '$adr',
                                style: GoogleFonts.quicksand(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    //------------------------------------------------//

                    payPageTwo
                        ? Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 28.0, top: 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Litti :  $littiNumber',
                                    style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    'Bharta :  $bhartaNumber',
                                    style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    'Total ₹ : $price',
                                    style: GoogleFonts.quicksand(),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 28.0, top: 0),
                              child: Column(
                                children: [
                                  Text(
                                    'Plates order :  $qt',
                                    style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    'Total ₹ : $price x $qt = ₹${price * qt}',
                                    style: GoogleFonts.quicksand(),
                                  ),
                                ],
                              ),
                            ),
                          ),

                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.1,
                    )

                    //------------------------------------------//
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                  ),
                  child: Center(
                    child: internetConnected
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              isOrdering
                                  ? Text(
                                      'Order Processing...',
                                      style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                        fontStyle: FontStyle.italic,
                                      ),
                                    )
                                  : Row(
                                      children: <Widget>[
                                        Text(
                                          'Cash on delivery',
                                          style: GoogleFonts.quicksand(
                                              fontWeight: FontWeight.w500,
                                              color: Colors.white),
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            cashOnDel
                                                ? Icons.check_box
                                                : Icons.check_box_outline_blank,
                                            color: Colors.white,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              if (cashOnDel == false) {
                                                cashOnDel = true;
                                              } else {
                                                cashOnDel = false;
                                              }
                                            });
                                          },
                                        )
                                      ],
                                    ),

                              //-------------------------<><><><><><><><><><>-------------------//

                              isOrdering
                                  ? CircularProgressIndicator(
                                      backgroundColor: Colors.green,
                                    )
                                  // ignore: deprecated_member_use
                                  : FlatButton(
                                      splashColor: Colors.green,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: BorderSide(
                                          color: Colors.yellowAccent[400],
                                        ),
                                      ),
                                      onPressed: () async {
                                        if (cashOnDel == false) {
                                          openCheckout();
                                        } else {
                                          orderId = uuid.v4();
                                          print(orderId);
                                          setState(() {
                                            if (isOrdering == false) {
                                              isOrdering = true;
                                            }
                                          });
                                          payPageTwo
                                              ? await addorder(
                                                  mobile: mobile,
                                                  name: name,
                                                  adr: adr,
                                                  littiNumber: littiNumber,
                                                  bhartaNumber: bhartaNumber,
                                                  total: price,
                                                  uid: auth.currentUser.uid,
                                                  orderId: orderId,
                                                )
                                              : await addorder(
                                                  mobile: mobile,
                                                  name: name,
                                                  adr: adr,
                                                  plates: qt,
                                                  total: total,
                                                  uid: auth.currentUser.uid,
                                                  orderId: orderId,
                                                );

                                          var timer = Timer.periodic(
                                              Duration(seconds: 1),
                                              (Timer t) async {
                                            getOrderData();
                                          });

                                          timerControl = timer;
                                        }
                                      },
                                      child: Container(
                                        child: Center(
                                          child: Text(
                                            cashOnDel
                                                ? 'Place order'
                                                : 'Pay Online',
                                            style: GoogleFonts.quicksand(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        height: 30,
                                        width: 90,
                                        decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius:
                                              BorderRadius.circular(40),
                                        ),
                                      ),
                                    ),

                              //------------<><><><<>><><><><><><><><><>>><><>--------------//
                            ],
                          )
                        : Text(
                            'No Internet Found!',
                            style: TextStyle(color: Colors.red),
                          ),
                  ),
                ),
              ),
              //
            ],
          ),
        ),
      );
  }
}
