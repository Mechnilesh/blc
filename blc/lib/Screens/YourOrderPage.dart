// import 'package:blc/FunctionsFirestore/userFunctions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../main.dart';
import 'CustomWidgets/MyOrder.dart';
// import 'Payment.dart';

int myOrderPrice, myOrderPlates, myOrderLittiNumber, myOrderBhartaNumber;
String myOrderMobile;

class YourOrderPage extends StatefulWidget {
  @override
  _YourOrderPageState createState() => _YourOrderPageState();
}

class _YourOrderPageState extends State<YourOrderPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellowAccent[400],
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        elevation: 0,
        title: Text(
          'Your orders',
          style: GoogleFonts.quicksand(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Container(
            height: 30,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 0.5,
                ),
              ],
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 16,
              ),
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
            ),
          ),
          MyOrderInformation(),
        ],
      ),
    );
  }
}

// int myOrderPrice, myOrderPlates, myOrderLittiNumber, myOrderBhartaNumber;

class MyOrderInformation extends StatefulWidget {
  @override
  _MyOrderInformationState createState() => _MyOrderInformationState();
}

class _MyOrderInformationState extends State<MyOrderInformation> {
  @override
  Widget build(BuildContext context) {
    Query orders = FirebaseFirestore.instance
        .collection('users')
        .doc(auth.currentUser.uid)
        .collection('myOrders')
        .orderBy('timestamp', descending: true);

    return StreamBuilder<QuerySnapshot>(
      stream: orders.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }
        // if (snapshot.connectionState == ConnectionState.active) {
        //   return Center(child: Icon(Icons.hourglass_empty));
        // }
        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new MyOrderCard(
              bhartaNumber: document.data()['bhartaNumber'],
              littiNumber: document.data()['littiNumber'],
              qty: document.data()['orderplates'],
              total: document.data()['total'],
              adr: document.data()['orderadr'],
              mobile: document.data()['ordermobile'],
              name: document.data()['name'],
              orderId: document.data()['orderId'],
              orderAccepted: document.data()['accepted'],
              orderRejected: document.data()['rejected'],
              time: document.data()['timestamp'].toDate(),
              // firebaseCountDown: document.data()['countDown'],
            );
          }).toList(),
        );
      },
    );
  }
}
