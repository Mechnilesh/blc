import 'package:blc/FunctionsFirestore/userFunctions.dart';
import 'package:blc/Screens/CustomWidgets/SplashScreen.dart';
import 'package:blc/Screens/HomePage.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'dart:io';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info/package_info.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

FirebaseAuth auth = FirebaseAuth.instance;
FirebaseFirestore firestore = FirebaseFirestore.instance;
bool status;
bool internetConnected = false;
int ibn, ipp, iln, ibp, ilp;

//---------------------------Update_PopUP-----------------------------------//

const PLAY_STORE_URL =
    'https://play.google.com/store/apps/details?id=com.mechnilesh.blc';

versionCheck(context) async {
  //Get Current installed version of app
  final PackageInfo info = await PackageInfo.fromPlatform();
  double currentVersion = double.parse(info.version.trim().replaceAll(".", ""));
  print("current app v" + info.version);

  //Get Latest version info from firebase config
  final RemoteConfig remoteConfig = await RemoteConfig.instance;
  print(remoteConfig.getString('force_update_current_version'));

  try {
    // Using default duration to force fetching from remote server.
    await remoteConfig.fetch(expiration: const Duration(seconds: 0));
    await remoteConfig.activateFetched();
    remoteConfig.getString('force_update_current_version');
    double newVersion = double.parse(remoteConfig
        .getString('force_update_current_version')
        .trim()
        .replaceAll(".", ""));
    if (newVersion > currentVersion) {
      _showVersionDialog(context);
    }
  } on FetchThrottledException catch (exception) {
    // Fetch throttled.
    print(exception);
  } catch (exception) {
    print('Unable to fetch remote config. Cached or default values will be '
        'used');
  }
}

_showVersionDialog(context) async {
  await showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      String title = "New Update Available";
      String message =
          "There is a newer version of app available please update it now.";
      String btnLabel = "Update Now";

      return Platform.isIOS
          ? WillPopScope(
            onWillPop: () async => false,
                      child: new CupertinoAlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => _launchURL(""),
                  ),
                  // ignore: deprecated_member_use
                ],
              ),
          )
          : WillPopScope(
              onWillPop: () async => false,
              child: new AlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => _launchURL(PLAY_STORE_URL),
                  ),
                  // ignore: deprecated_member_use
                ],
              ),
            );
    },
  );
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

//-----------------------------<><>><><UpdatePopUp><><><><--------------------------//

getThisData() async {
  FirebaseFirestore.instance
      .collection('plateSetting')
      .doc('7523RFH7SwOSqBrHWyvI')
      .get()
      .then((DocumentSnapshot documentSnapshot) {
    if (documentSnapshot.exists) {
      ibn = documentSnapshot.data()['bharta'];

      ipp = documentSnapshot.data()['price'];

      iln = documentSnapshot.data()['litti'];
    } else {
      print('Document does not exist on the database');
    }
  });

  FirebaseFirestore.instance
      .collection('customSetting')
      .doc('cgyG9D7Z7P0JSHuMUDjl')
      .get()
      .then((DocumentSnapshot documentSnapshot) {
    if (documentSnapshot.exists) {
      ibp = documentSnapshot.data()['bharta'];

      ilp = documentSnapshot.data()['litti'];
    } else {
      print('Document does not exist on the database');
    }
  });
}

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _initialized = false;
  bool _error = false;

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_error) {
      return Center(
        child: Text("Something Went Wrong"),
      );
    }
    if (!_initialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.yellowAccent[400],
      ),
      home: Scaffold(
        backgroundColor: Colors.yellowAccent[400],
        body: SplashScreen(),
      ),
    );
  }
}

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  bool isSignIn = false;
  bool isProgress = false;
  bool userIslogedIn = false;
  bool _validate = false;
  bool emailNotValid = false;
  bool fillEmailForReset = false;
  bool obsecureText = true;
  bool isForgotLoading = false;

  String email = "";
  String password = "";
  String name = "";

  final _name = TextEditingController();
  final _email = TextEditingController();
  final _password = TextEditingController();

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen((User user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        print('User is signed in!');
        print(auth.currentUser.uid);
        setState(() {
          userIslogedIn = true;
        });
      }
    });
    try {
      versionCheck(context);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return userIslogedIn
        ? HomePage()
        : Scaffold(
            backgroundColor: Colors.yellowAccent[400],
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/wlscreen.png",
                    height: MediaQuery.of(context).size.height * 0.4,
                  ),
                  isSignIn
                      ? Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              TextField(
                                keyboardType: TextInputType.emailAddress,
                                controller: _email,
                                onChanged: (value) {
                                  this.email = value.trimLeft();
                                  emailNotValid =
                                      EmailValidator.validate(email);
                                  // print(emailNotValid);
                                },
                                decoration: InputDecoration(
                                  hintText: fillEmailForReset
                                      ? "Your email id is required to send reset link"
                                      : "Email",
                                  hintStyle: GoogleFonts.quicksand(
                                    color: fillEmailForReset
                                        ? Colors.red
                                        : Colors.black54,
                                  ),
                                ),
                              ),
                              TextField(
                                controller: _password,
                                obscureText: obsecureText,
                                onChanged: (value) {
                                  this.password = value.trimLeft();
                                },
                                decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: GoogleFonts.quicksand(),
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.remove_red_eye,
                                      color: obsecureText
                                          ? Colors.black54
                                          : Colors.blue,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (obsecureText) {
                                          obsecureText = false;
                                        } else {
                                          obsecureText = true;
                                        }
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              TextField(
                                keyboardType: TextInputType.emailAddress,
                                controller: _email,
                                onChanged: (value) {
                                  email = value.trimLeft();
                                  emailNotValid =
                                      EmailValidator.validate(email);
                                },
                                decoration: InputDecoration(
                                  hintText: fillEmailForReset
                                      ? "Your email id is required to send reset link"
                                      : "Email",
                                  hintStyle: GoogleFonts.quicksand(
                                    color: fillEmailForReset
                                        ? Colors.red
                                        : Colors.black54,
                                  ),
                                ),
                              ),
                              TextField(
                                obscureText: obsecureText,
                                controller: _password,
                                onChanged: (value) {
                                  password = value.trimLeft();
                                },
                                decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: GoogleFonts.quicksand(),
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.remove_red_eye,
                                      color: obsecureText
                                          ? Colors.black54
                                          : Colors.blue,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (obsecureText) {
                                          obsecureText = false;
                                        } else {
                                          obsecureText = true;
                                        }
                                      });
                                    },
                                  ),
                                ),
                              ),
                              TextField(
                                controller: _name,
                                onChanged: (value) {
                                  this.name = value.trimLeft();
                                },
                                decoration: InputDecoration(
                                  hintText: "Full Name",
                                  hintStyle: GoogleFonts.quicksand(),
                                ),
                              ),
                            ],
                          ),
                        ),
                  isSignIn
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Don't have an account ? ",
                              style: GoogleFonts.quicksand(),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isSignIn = false;
                                });
                              },
                              child: Text(
                                "Sign Up",
                                style: GoogleFonts.quicksand(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Already have an account ! ",
                              style: GoogleFonts.quicksand(),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isSignIn = true;
                                  _validate = false;
                                });
                              },
                              child: Text(
                                "Sign In",
                                style: GoogleFonts.quicksand(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ],
                        ),
                  isForgotLoading
                      ? Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                        )
                      : GestureDetector(
                          onTap: () async {
                            if (email.isEmpty) {
                              setState(() {
                                fillEmailForReset = true;
                              });
                            } else {
                              setState(() {
                                isForgotLoading = true;
                              });
                              await FirebaseAuth.instance
                                  .sendPasswordResetEmail(email: email);
                              await Future.delayed(
                                Duration(seconds: 10),
                              );
                              setState(() {
                                isForgotLoading = false;
                              });
                            }
                          },
                          child: Text(
                            "Forgot Password ? ",
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                  isForgotLoading
                      ? Text(
                          "Check your Email-id",
                          style: GoogleFonts.quicksand(
                            color: Colors.black,
                          ),
                        )
                      : Container(),
                  _validate
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Center(
                            child: Text(
                              "Fields can't be empty",
                              style: TextStyle(
                                color: Colors.red,
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  Align(
                    alignment: Alignment.bottomRight,
                    heightFactor: 3,
                    child: GestureDetector(
                      onTap: isSignIn
                          ? () async {
                              if (email.isEmpty ||
                                  password.isEmpty ||
                                  emailNotValid == false) {
                                setState(() {
                                  _validate = true;
                                });
                              } else {
                                setState(() {
                                  _validate = false;
                                  isProgress = true;
                                });
                                try {
                                  await FirebaseAuth.instance
                                      .signInWithEmailAndPassword(
                                    email: this.email,
                                    password: this.password,
                                  );
                                } on FirebaseAuthException catch (e) {
                                  if (e.code == 'user-not-found') {
                                    print('No user found for that email.');
                                    setState(() {
                                      isProgress = false;
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text('Please create account'),
                                        // duration: ,
                                      ),
                                    );
                                  } else if (e.code == 'wrong-password') {
                                    print(
                                        'Wrong password provided for that user.');
                                    setState(() {
                                      isProgress = false;
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            'Wrong password provided for that user'),
                                        // duration: ,
                                      ),
                                    );
                                  }
                                }
                              }
                            }
                          : () async {
                              if (name.isEmpty ||
                                  email.isEmpty ||
                                  password.isEmpty ||
                                  emailNotValid == false) {
                                setState(() {
                                  _validate = true;
                                });
                              } else {
                                setState(() {
                                  _validate = false;
                                  isProgress = true;
                                });
                                try {
                                  await FirebaseAuth.instance
                                      .createUserWithEmailAndPassword(
                                    email: this.email,
                                    password: this.password,
                                  );
                                  addUser(this.name, auth.currentUser.uid,
                                      this.email);
                                } on FirebaseAuthException catch (e) {
                                  if (e.code == 'weak-password') {
                                    print('The password provided is too weak.');
                                    setState(() {
                                      isProgress = false;
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            'The password provided is too weak or empty'),
                                      ),
                                    );
                                  } else if (e.code == 'email-already-in-use') {
                                    print(
                                        'The account already exists for that email.');
                                    setState(() {
                                      isProgress = false;
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                          'The account already exists for that email.',
                                        ),
                                      ),
                                    );
                                  }
                                } catch (e) {
                                  print(e);
                                }
                              }
                            },
                      child: Container(
                        height: 40,
                        width: 150,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            topLeft: Radius.circular(20),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: .5,
                            ),
                          ],
                        ),
                        child: isProgress
                            ? Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: LinearProgressIndicator(
                                  backgroundColor: Colors.white,
                                ),
                              )
                            : Align(
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    isSignIn
                                        ? Text(
                                            "Sign In",
                                            style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        : Text(
                                            "Sign Up",
                                            style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                    Icon(Icons.arrow_forward_ios)
                                  ],
                                ),
                              ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }
}
